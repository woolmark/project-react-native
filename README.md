
Project Woolam with React Native for iOS is a React Native/Flux iOS Application.

# Development

## Requires
 - Xcode 6.3
 - Node.js, node and npm commands
 - React Native CLI

## Setup development environment
Run a commad in the project direcotry

    % npm install

## Build for development
Uncomment a bundle script file name to `http://localhost:...` from `.jsbundle` in `AppDelegate.m`

Before

    // jsCodeLocation = [NSURL URLWithString:@"http://localhost:8081/index.ios.bundle"];
    jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];

After

    jsCodeLocation = [NSURL URLWithString:@"http://localhost:8081/index.ios.bundle"];
    // jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];

And, build the application with Xcode, Cmd-B or Product > Build


# Release
## Build for production
Uncomment a bundle script file name to `.jsbundle` from `http://localhost:...` in `AppDelegate.m`

And, run a command to update a minimized bundle script

    % react-native bundle --minify


