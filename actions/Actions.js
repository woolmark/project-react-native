
var Dispatcher = require('../dispatcher/Dispatcher');
var ActionType = require('../constants/ActionType');

var VALID_NAME_PREFIX = [ 'sheep-', 'woolmark-' ];

var Actions = {

  show: function(content) {
    Dispatcher.dispatch({
      actionType: ActionType.ShowContent,
      content: content
    });
  },

  fetchRepository: function(url) {
    fetch(url)
        .then((response) => {
          if (response.status >= 200 && response.status < 300) {
            return response.json();
          }
          throw new Error(response.status);
        })
        .then((responseData) => {
          // Filter repos for valid prefix
          var repos = [];
          responseData.values.reverse().map(function(repo) {
            for (var i = 0; i < VALID_NAME_PREFIX.length; i++) {
              if (repo.name.startsWith(VALID_NAME_PREFIX[i])) {
                repos.push(repo);
                break;
              }
            }
          });

          Dispatcher.dispatch({
            actionType: ActionType.UpdateRepositories,
            repos: repos
          });
        })
        .catch((status) => {
          Dispatcher.dispatch({
            actionType: ActionType.FailUpdateRepositories,
          });
        });
  }

};

module.exports = Actions;

