'use strict';

var React = require('react-native');
var {
  StyleSheet,
  ScrollView,
  Text,
  Image,
  View,
  TouchableOpacity,
  LinkingIOS,
} = React;

var Menu = require('./Menu.react');
var SheepView = require('../components_native/SheepView');
var styles = require('./Styles.react');
var ContentName = require('../constants/ContentName');

var SHEEPVIEW_REF = 'sheepview';

var About = React.createClass({

  getDefaultProps: function() {
    return {
      title: 'About'
    };
  },

  render: function() {
    return (
      <ScrollView style={styles.content}
          automaticallyAdjustContentInsets={false} >
        <WhatIs />
        <Goal />
        <Contact />
      </ScrollView>
    );
  }
});

var WhatIs = React.createClass({
  render: function() {
    return (
      <View>
      <Text style={styles.section}>What is</Text>

      <View style={styles.paragraph}>
      <Text style={styles.text}>
        Sheep and WoolMark are the simple applications to count running sheep.
      </Text>
      </View>

      <View style={styles.paragraph}>
        <SheepView style={aboutStyles.sheep}
            ref={SHEEPVIEW_REF} />
      </View>

      </View>
    );
  }
});

var Goal = React.createClass({
  render: function() {
    return (
      <View>
      <Text style={styles.section}>Goal</Text>
      <View style={styles.paragraph}>
      <Text style={styles.text}>
        There is no goal. We will create new sheep, if there will be new platform.
      </Text>
      </View>
      </View>
    );
  }
});

var Contact = React.createClass({
  render: function() {
    return (
      <View>
        <Text style={styles.section}>Contact</Text>
        <View style={styles.paragraph}>
        <TouchableOpacity onPress={this.onClickMail}>
        <Text style={styles.link}>
          team.woolmark [at] gmail.com
        </Text>
        </TouchableOpacity>
        </View>
      </View>
    );
  },
  onClickMail: function() {
    LinkingIOS.openURL('mailto:team.woolmark@gmail.com');
  }
});

var aboutStyles = StyleSheet.create({

  sheep: {
    width: 120,
    height: 120
  }

});

module.exports = About;

