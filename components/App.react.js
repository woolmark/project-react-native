
var React = require('react-native');
var {
  StyleSheet,
  NavigatorIOS
} = React;

var Menu = require('./Menu.react');
//var Footer = require('./Footer.react');

var Woolmark = React.createClass({
  render: function() {
    return (
      <NavigatorIOS
          style={styles.main}
          initialRoute={{
            title: 'Project Woolmark',
            component: Menu
          }}/>     
    );
  }
});

var styles = StyleSheet.create({
  main: {
    flex: 1
  }
});

module.exports = Woolmark;

