
var React = require('react-native');
var {
  StyleSheet,
  ScrollView,
  Text,
  View,
  TouchableOpacity,
  LinkingIOS,
} = React;

var Menu = require('./Menu.react');
var styles = require('./Styles.react');
var ContentName = require('../constants/ContentName');

var FAQ = React.createClass({

  getDefaultProps: function() {
    return {
      title: 'FAQ'
    };
  },

  render: function() {
    return (
      <ScrollView style={styles.content}
          automaticallyAdjustContentInsets={false} >
        <Repository />
        <License />
        <DiffSheep />
      </ScrollView>
    );
  }
});

var Repository = React.createClass({
  render: function() {
    return (
      <View>
      <Text style={styles.section}>Where is development repository?</Text>
      <View style={styles.paragraph}>
      <Text style={styles.text}>
      Development repository is hosted in bitbucket.
      </Text>
      </View>

      <View style={styles.paragraph}>
      <TouchableOpacity onPress={this.onClickWoolmark}>
        <Text style={styles.link}>
        http://www.bitbucket.org/woolmark/
        </Text>
      </TouchableOpacity>
      </View>

      </View>
    );
  },
  onClickWoolmark: function() {
    LinkingIOS.openURL('http://woolmark.bitbucket.org');
  }
});

var License = React.createClass({
  render: function() {
    return (
      <View>

      <Text style={styles.section}>What is license?</Text>
      <View style={styles.paragraph}>
      <Text style={styles.text}>
      It is WTFPL. It is not commercial and not virus license.
      </Text>
      </View>

      <View style={styles.paragraph}>
      <TouchableOpacity onPress={this.onClickWTFPL}>
        <Text style={styles.link}>
        http://www.wtfpl.net/
        </Text>
      </TouchableOpacity>
      </View>

      </View>
    );
  },
  onClickWTFPL: function() {
    LinkingIOS.openURL('http://www.wtfpl.net');
  }
});

var DiffSheep = React.createClass({
  render: function() {
    return (
      <View>

      <Text style={styles.section}>What's difference between sheep and woolmark?</Text>
      <View style={styles.paragraph}>
      <Text style={styles.text}>
      Sheep is an simple application to run the sheep, WoolMark is a bench mark application to run the sheep.
      </Text>
      </View>

      </View>
    );
  }
});

module.exports = FAQ;

