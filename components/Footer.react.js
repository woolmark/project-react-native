
var React = require('react');

var Footer = React.createClass({ className: 'Footer',
    render: function() {
        return (
            <div id="footer">
            <hr />
            Copyright &copy; 2013 - 2015 <a href="http://woolmark.bitbucket.org/">Team WoolMark</a>
             &nbsp;|&nbsp;<a id="privacypolicy_link" href="privacypolicy.html">Privacy Policy</a>
            </div>
        );
    }
});

module.exports = Footer;

