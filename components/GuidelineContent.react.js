
var React = require('react-native');
var {
  StyleSheet,
  ScrollView,
  Image,
  Text,
  View,
} = React;

var Menu = require('./Menu.react');
var styles = require('./Styles.react');
var ContentName = require('../constants/ContentName');

var Guideline = React.createClass({

  getDefaultProps: function() {
    return {
      title: 'Guideline'
    };
  },

  render: function() {
    return (
      <ScrollView style={styles.content}
          automaticallyAdjustContentInsets={false} >
        <Feature />
        <Logic />
      </ScrollView>
    );
  }
});

var Feature = React.createClass({
  render: function() {
    return (
      <View>
      <Text style={styles.section}>Feature</Text>
      <View style={styles.paragraph}>
        <Text style={styles.text}>・Run and jump the sheep</Text>
        <Text style={styles.text}>・Jumped sheep is counted</Text>
      </View>
      </View>
    );
  }
});

var Logic = React.createClass({
  render: function() {
    return (
      <View>
      <Text style={styles.section}>Logic</Text>

      <Text style={styles.subsection}>Canvas Size</Text>

      <View style={styles.paragraph}>
      <Text style={styles.text}>
      The default canvas size is `120x120`. This application is not enable resizing.
      The image is scaled by integral multiplication if the canvas size is enable resizing.
      </Text>
      </View>

      <Text style={styles.subsection}>Color</Text>

      <View style={styles.paragraph}>
      <Text>
      These colors are one of better color set.
      </Text>
      </View>

      <View style={styles.paragraph}>
        <Text>・Sky : RGB(100, 255, 100)</Text>
        <Text>・Ground : RGB(150, 150, 255)</Text>
      </View>

      <View style={styles.paragraph}>
      <Text>
        It will be changed if the platform is not supported RGB888.
      </Text>
      </View>

      <Text style={styles.subsection}>Images</Text>

      <View style={styles.paragraph}>
      <Text>
      The images should be drawn by the following image resources.
      You maybe draw by yourself, if drawing image function does not exist on implementation platform.
      </Text>
      </View>

      <Text style={styles.subsubsection}>Sheep</Text>
      <View style={styles.paragraph}>
      <Image style={styles.image} source={require('image!sheep00')} />
      <Image style={styles.image} source={require('image!sheep01')} />
      </View>

      <Text style={styles.subsubsection}>Fence</Text>
      <View style={styles.paragraph}>
      <Image style={styles.image} source={require('image!fence')} />
      </View>

      <Text>
      </Text>

      <Text style={styles.subsection}>Ground</Text>

      <Text style={styles.subsubsection}>Height</Text>

      <View style={styles.paragraph}>
      <Text>
      Ground height should be less than the fence height.
      </Text>
      </View>

      <Text style={styles.subsection}>Fence</Text>

      <Text style={styles.subsubsection}>Position</Text>

      <View style={styles.paragraph}>
      <Text>
      The fence is on bottom and horizontal center.
      </Text>
      </View>

      <Text style={styles.subsection}>Sheep</Text>

      <Text style={styles.subsubsection}>Run and Jump</Text>

      <View style={styles.paragraph}>
      <Text>
      The sheep runs from right to left on the ground, and jumps when a sheep goes over the fence.
      </Text>
      </View>

      <Text style={styles.subsubsection}>Animation</Text>

      <View style={styles.paragraph}>
      <Text>
      Sheep image flips two images frame by frame.
      The images are a stretch and not a stretch. It's always stretch if a sheep is jumping.
      </Text>
      </View>

      <Text style={styles.subsubsection}>Append</Text>

      <View style={styles.paragraph}>
      <Text>
      The sheep append is started when user presses the left mouse button.
      And the sheep append is stopped when user releases the left mouse button.
      </Text>
      </View>

      <View style={styles.paragraph}>
      <Text>
      The left mouse button is replaced CENTER key or display touch,
      if there is not the mouse user interface.
      </Text>
      </View>

      <View style={styles.paragraph}>
      <Text>
      The appended sheep runs away when it moves to left edge.
      </Text>
      </View>

      <Text style={styles.subsection}>Counter</Text>

      <View style={styles.paragraph}>
      <Text>
      The sheep counter is on left upper on the canvas.
      It counts up when a sheep jumps over the fence.
      </Text>
      </View>

      <View style={styles.paragraph}>
      <Text>
      The number is loaded when the application is launched,
      and is saved when the application is finished.
      If the application does not have any storage to save the data, that function may not be implemented.
      </Text>
      </View>

      </View>
    );
  }
});

module.exports = Guideline;

