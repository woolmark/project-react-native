
var React = require('react-native');
var {
  StyleSheet,
  View,
  Text,
  Image
} = React;

var Menu = require('./Menu.react');

var Home = React.createClass({
  render: function() {
    return (
      <View style={styles.container}>
      <View style={styles.content}>
        <Logo />
        <Summary />
      </View>
      <View style={styles.bottom}>
          <Menu />
      </View>
      </View>
    );
  }
});

var Logo = React.createClass({
  render: function() {
    return (
      <Image source={require('image!avatar')} style={styles.logo} />
    );
  }
});

var Summary = React.createClass({
  render: function() {
    return (
      <View>
        <Text style={styles.title}>Sheep and WoolMark</Text>
        <Text style={styles.summary}>A simple application, running sheep counter.</Text>
      </View>
    );
  }
});

var styles = StyleSheet.create({

  container: {
    flex: 1,
  },

  bottom: {
      height: 48
  },

  content: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  logo: {
    width: 120,
    height: 120,
  },

  title: {
    fontSize: 22,
    textAlign: 'center',
    marginTop: 20
  },

  summary: {
    fontSize: 16,
    textAlign: 'center',
    marginTop: 8
  }

});

module.exports = Home;

