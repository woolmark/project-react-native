
var React = require('react-native');
var {
  StyleSheet,
  View,
  TabBarIOS,
  Text,
  Image
} = React;

var About = require('./AboutContent.react');
var Guideline = require('./GuidelineContent.react');
var Projects = require('./ProjectsContent.react');
var FAQ = require('./FAQContent.react');

var ContentStore = require('../stores/ContentStore');
var Actions = require('../actions/Actions');
var ActionType = require('../constants/ActionType');
var ContentName = require('../constants/ContentName');

var Menu = React.createClass({

  getInitialState: function() {
    var content = ContentStore.getCurrentContent();
    if (!content) {
        content = ContentName.About;
    }

    return {
      content: content
    };
  },

  componentDidMount: function() {
    ContentStore.addChangeListener(this._onContentChange);
  },

  componentWillUnmount: function() {
    ContentStore.removeChangeListener(this._onContentChange);
  },

  render: function() {

    var title;
    var content = this.state.content;

    // Insert Menu and Footer if needed
    return (
      <TabBarIOS>
        <TabBarIOS.Item
            systemIcon='featured'
            selected={content === ContentName.About}
            onPress={() => {
              Actions.show(ContentName.About);
            }}>
          <View style={styles.container}>
            <About />
          </View>
        </TabBarIOS.Item>
        <TabBarIOS.Item
            title='Guideline'
            icon={require('image!About')}
            selected={content === ContentName.Guideline}
            onPress={() => {
              Actions.show(ContentName.Guideline);
            }}>
          <View style={styles.container}>
            <Guideline />
          </View>
        </TabBarIOS.Item>
        <TabBarIOS.Item
            title='Projects'
            icon={require('image!List')}
            selected={content === ContentName.Projects}
            onPress={() => {
                Actions.show(ContentName.Projects);
            }}>
          <View style={styles.container}>
            <Projects />
          </View>
        </TabBarIOS.Item>
        <TabBarIOS.Item
            systemIcon='more'
            selected={content === ContentName.FAQ}
            onPress={() => {
                Actions.show(ContentName.FAQ);
            }}>
          <View style={styles.container}>
            <FAQ />
          </View>
        </TabBarIOS.Item>
      </TabBarIOS>
    );
  },

  _onContentChange: function(event) {
    this.setState({
      content: ContentStore.getCurrentContent()
    });
  }

});

var styles = StyleSheet.create({

  main: {
    flex: 1
  },

  container: {
    flex: 1,
    paddingTop: 64,
    marginBottom: 50
  }

});

module.exports = Menu;

