'use strict';

var React = require('react-native');
var {
  StyleSheet,
  ScrollView,
  ListView,
  Image,
  Text,
  View,
  TouchableOpacity,
  LinkingIOS,
} = React;

var Menu = require('./Menu.react');
var styles = require('./Styles.react');
var Actions = require('../actions/Actions');
var ContentName = require('../constants/ContentName');
var RepositoryStore = require('../stores/RepositoryStore');

var Projects = React.createClass({

  getDefaultProps: function() {
    return {
      title: 'Projects',
    };
  },

  getInitialState: function() {
    var ds = new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2
    });

    if (RepositoryStore.hasRepositories()) {
      ds.cloneWithRows(RepositoryStore.getRepositories());
    }

    return {
      repos: ds,
      stored: RepositoryStore.hasRepositories(),
      loading: false
    };
  },

  componentDidMount: function() {
    if (!this.state.loaded) {
      this._fetchRepository();
    }
    RepositoryStore.addChangeListener(this._onRepositoryChange);
  },

  componentWillUnmount: function() {
    RepositoryStore.removeChangeListener(this._onRepositoryChange);
  },

  render: function() {

    if (this.state.loading) {
      // Show loading message
      return (
        <View style={projectStyles.loading}>
          <Text style={projectStyles.loadingText}>Now loading...</Text>
        </View>
      );
    } else if (!this.state.stored) {
      // Show reload button
      return (
        <View style={projectStyles.loading}>
          <TouchableOpacity onPress={this._fetchRepository}>
          <Text style={styles.link}>Reload</Text>
          </TouchableOpacity>
        </View>
      );
    } else {
      // Show repository list
      return (
        <ListView
          automaticallyAdjustContentInsets={false}
          dataSource={this.state.repos}
          renderRow={(repo) => <Project data={repo} /> } />
      );
    }

  },

  _fetchRepository: function() {
    this.setState({
      repos: this.state.repos,
      stored: false,
      loading: true
    });
    Actions.fetchRepository(RepositoryStore.getNextUrl());
  },

  _onRepositoryChange: function() {

    var ds = this.state.repos;
    if (RepositoryStore.hasRepositories()) {
      ds = this.state.repos.cloneWithRows(RepositoryStore.getRepositories());
      this.setState({
        repos: ds,
        stored: true,
        loading: false
      });
    } else {
      this.setState({
        repos: ds,
        stored: false,
        loading: false
      });
    }

  }

});

var Project = React.createClass({
  render: function() {

    var lastUpdateMillis = Date.parse(this.props.data.updated_on);
    var lastUpdate = new Date();
    lastUpdate.setTime(lastUpdateMillis);

    return (
      <TouchableOpacity onPress={this.onClickItem}>
      <View>

      <View style={projectStyles.listItem}>
        <Image style={projectStyles.thumbnail}
            source={{uri: this.props.data.links.avatar.href}} />
        <View style={projectStyles.description}>
          <Text style={projectStyles.title}>{this.props.data.name}</Text>
          <Text style={projectStyles.summary}>{lastUpdate.toString()}</Text>
        </View>
      </View>
      <View style={projectStyles.seperator} />

      </View>
      </TouchableOpacity>
    );

  },
  onClickItem: function() {
    LinkingIOS.openURL(this.props.data.links.html.href);
  }
});

var projectStyles = StyleSheet.create({

  listItem: {
    flex: 1,
    flexDirection: 'row',
  },

  thumbnail: {
    width: 32,
    height: 32,
    margin: 12
  },

  description: {
    flex: 1,
    marginTop: 8
  },

  title: {
    fontSize: 18
  },

  summary: {
    marginTop: 4,
    fontSize: 12,
    color: '#7C7C82'
  },

  seperator: {
    height: 1,
    backgroundColor: '#E7E7E7'
  },

  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },

  loadingText: {
    color: '#777777',
    fontSize: 16
  }

});

module.exports = Projects;

