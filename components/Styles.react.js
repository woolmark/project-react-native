'use strict';

var React = require('react-native');
var {
  StyleSheet
} = React;

var styles = StyleSheet.create({

  content: {
    flex: 1,
    paddingLeft: 12,
    paddingRight: 12
  },

  section: {
    fontSize: 18,
    marginTop: 12
  },

  subsection: {
    fontSize: 16,
    marginTop: 8
  },

  subsubsection: {
    fontSize: 14,
    marginTop: 4
  },

  paragraph: {
    marginTop: 8,
    marginBottom: 8
  },

  text: {
    fontSize: 14
  },

  image: {
    margin: 8
  },

  link: {
    fontSize: 14,
    color: '#0e6dfc'
  }

});

module.exports = styles;

