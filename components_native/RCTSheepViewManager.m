//
//  RCTSheepViewManager.m
//  ProjectWoolmark
//
//  Created by Naoki Takimura on 5/11/15.
//  Copyright (c) 2015 Facebook. All rights reserved.
//

#import "RCTSheepViewManager.h"
#import "RCTBridge.h"
#import "RCTSparseArray.h"
#import "RCTUIManager.h"

#import "SheepView.h"

@interface RCTSheepViewManager ()

@property (nonatomic, readwrite) SheepView *view;

@end

@implementation RCTSheepViewManager

RCT_EXPORT_MODULE()

- (UIView *)view
{
  if (!_view) {
    _view = [[SheepView alloc] init];
  }
  return _view;
}

@end
