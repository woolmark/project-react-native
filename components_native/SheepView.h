//
//  SheepView.h
//  sheep
//
//  Created by Takimura Naoki on 2013/06/01.
//  Copyright (c) 2013年 Takimura Naoki. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 * sheep view.
 *
 */
@interface SheepView : UIView

/**
 * scale to draw.
 */
@property NSInteger scale;

/**
 * sheep position array.
 */
@property NSMutableArray *sheep_pos;

/**
 * add sheep flag.
 *
 * if the flag is true, it adds a sheep per one frame.
 * if user touch down, the flag is true.
 * if not, the flag is false.
 */
@property bool sheep_add;

/**
 * sheep number counter.
 */
@property int sheep_number;

/**
 * the image of fence.
 */
@property UIImage *fence;

/**
 * the image of sheep[0].
 */
@property UIImage *sheep00;

/**
 * the image of sheep[1].
 */
@property UIImage *sheep01;

/**
 * running timer.
 */
@property NSTimer *timer;

/**
 * action counter.
 */
@property int action;

/**
 * run the sheep.
 */
- (void)run;

/**
 * stop the sheep.
 */
- (void)stop;

@end
