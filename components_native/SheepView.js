'use strict';

var { requireNativeComponent } = require('react-native');

module.exports = requireNativeComponent('RCTSheepView', null);
