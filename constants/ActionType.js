
var keyMirror = require('keymirror');

module.exports = keyMirror({
    ShowContent: null,
    UpdateRepositories: null,
    FailUpdateRepositories: null
});


