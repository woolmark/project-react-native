
var keyMirror = require('keymirror');

module.exports = keyMirror({
    Home: null,
    About: null,
    Guideline: null,
    Projects: null,
    FAQ: null
});


