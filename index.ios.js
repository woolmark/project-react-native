'use strict';

var React = require('react-native');
var Woolmark = require('./components/App.react');

var {
  AppRegistry
} = React;

AppRegistry.registerComponent('ProjectWoolmark', () => Woolmark);

