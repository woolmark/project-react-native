
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var Dispatcher = require('../dispatcher/Dispatcher');
var ActionType = require('../constants/ActionType');
var ContentName = require('../constants/ContentName');

var content;

var SHOW_EVENT = 'show';

var ContentStore = assign({}, EventEmitter.prototype, {

    getCurrentContent: function() {
        return content;
    },

    emitChange: function() {
        this.emit(SHOW_EVENT);
    },

    addChangeListener: function(callback) {
        this.on(SHOW_EVENT, callback);
    },

    removeChangeListener: function(callback) {
        this.removeListener(SHOW_EVENT, callback);
    },

    dispatcherIndex: Dispatcher.register(function(payload) {

        switch (payload.actionType) {
        case ActionType.ShowContent:
            content = payload.content;
            ContentStore.emitChange();
            break;
        default:
            break;
        }

        return true;
    })

});

module.exports = ContentStore;


