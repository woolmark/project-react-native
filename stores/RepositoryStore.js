'use strict';

var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var Dispatcher = require('../dispatcher/Dispatcher');
var ActionType = require('../constants/ActionType');
var StaticRepositories = require('../constants/repositories.json');

var repos;
var stored;

var nextUrl = 'https://api.bitbucket.org/2.0/repositories/woolmark?pagelen=100';

var UPDATE_REPOSITORY = 'update';

var RepositoryStore = assign({}, EventEmitter.prototype, {

  hasRepositories: function() {
    return stored;
  },

  getRepositories: function() {
    return repos;
  },

  getNextUrl: function() {
    return nextUrl;
  },

  emitChange: function() {
    this.emit(UPDATE_REPOSITORY);
  },

  addChangeListener: function(callback) {
    this.on(UPDATE_REPOSITORY, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(UPDATE_REPOSITORY, callback);
  },

  dispatcherIndex: Dispatcher.register(function(payload) {

    switch (payload.actionType) {
    case ActionType.UpdateRepositories:
      repos = payload.repos;
      stored = true;
      RepositoryStore.emitChange();
      break;
    case ActionType.FailUpdateRepositories:
      stored = false;
      RepositoryStore.emitChange();
      break;
    default:
      break;
    }

    return true;
  })

});

module.exports = RepositoryStore;


